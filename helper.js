
module.exports = {
    getUnusedKeyId: getUnusedKeyId
};

/**
 * Function used to find a key in the assignment's key list data that isn't currently checked out
 *
 * @param requestParams - object given to the Request library. Use this parameter to customize what is sent in the request (headers, body, cookies etc)
 * @param response - the response object from the Request library. This object contains response headers, body etc.
 * @param context - the virtual user’s context, context.vars is a dictionary containing all defined variables
 * @param ee - an event emitter that can be used to communicate with Artillery
 * @param next - the callback which must be called for the scenario to continue; it takes no arguments
 * @returns {*}
 */
function getUnusedKeyId(requestParams, response, context, ee, next) {
    //empty response -- set variables to 0 and skip
    if(!response.body) {
        context.vars['assignmentId'] = 0;
        context.vars['keyId'] = 0;
        return next();
    }

    var body = JSON.parse(response.body);
    context.vars['assignmentId'] = body.id;

    var id = 0;
    var keys = body.keys;
    for(var keyIdx in keys) {
        if(keys.hasOwnProperty(keyIdx)) {
            var keyData = keys[keyIdx];
            console.log("KeyData:",keyData);
            //look for one with a null or 0 keyCheckoutId
            if(!keyData['keyCheckoutId']) {
                id = keyData.id;
                break;
            }
        }
    }

    console.log("Get Unused Key ID",id);
    if(!id) {
        id = keys[0] ? keys[0].id : 0;
    }
    context.vars['keyId'] = id;
    return next(); // MUST be called for the scenario to continue
}