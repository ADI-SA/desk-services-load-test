# Housing Desk Services Load Tests

## Overview

This repository contains a set of load tests for the Housing Desk Services application. It uses the [Artillery.io](https://artillery.io) load testing framework.

## Installation

Execute `npm install` in the project directory.

To run the tests against your local server, first check the config.environments.local.target URL in the tests/checkin.yml file, and make sure it's aimed at the port that your local server is running on.

Then simply execute `npm start` to run the load tests. When they are completed, the results page should open in your default web browser.

## Acceptance Criteria

From [JIRA ticket for load tests](https://jira.doit.wisc.edu/jira/browse/HOUSING-98)

Essential searches, e.g. resident look-up ([HOUSING-20](https://jira.doit.wisc.edu/jira/browse/HOUSING-20)) and transactions, e.g. issue key ([HOUSING-18](https://jira.doit.wisc.edu/jira/browse/HOUSING-18) and [HOUSING-21](https://jira.doit.wisc.edu/jira/browse/HOUSING-21)) should respond "quickly" under load. Run the search and one or the other transaction every 30 seconds, +/- up random up to five seconds, from each of 20 clients, for at least 10 minutes.

* Great response, <2 seconds
* Adequate response, < 5 seconds

(UX opinion welcome!)
Increase the number of clients until 100 (5x) or it crashes/freezes, whichever comes first. 